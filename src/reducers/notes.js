import { userConst } from '../constants/userConst';
import undoable, { includeAction } from 'redux-undo';

export function tasks (state = {}, action) {
    switch (action.type) {
        case userConst.GET_REQUEST:
            return {
                taskRequest: true
            };
        case userConst.GET_SUCCESS:
            return {
                redirectToTasks: true,
                tasks: action.tasks
            };
        case userConst.GET_FAILURE:
            return {};
        case userConst.ADD_REQUEST:
            return {
                addRequest: true
            };
        case userConst.ADD_SUCCESS:
            return {
                addSuccess: true 
            };
        case userConst.CHANGE_TITLE:
            return {
                ...state,
                title: action.title
            };
        case userConst.CHANGE_BODY:
            return {
                ...state,
                body: action.body
            };
        case userConst.ADD_FAILURE:
            return {};       
        case userConst.REMOVE_SUCCESS:
            return {
                ...state,
                tasks: state.tasks.filter(task => action.id !== task.id)
            };  
        default:
            return state
    }
}

const undoableTodos = undoable(tasks, { filter: includeAction([userConst.CHANGE_TITLE, userConst.CHANGE_BODY]),
    ignoreInitialState: true,
    neverSkipReducer: false,})

export default undoableTodos