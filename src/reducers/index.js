import { combineReducers } from 'redux'
import {auth} from '../reducers/auth'
import {register} from '../reducers/register'
import {tasks} from '../reducers/notes';

export default combineReducers({ 
    auth,
    register,
    tasks
}) 