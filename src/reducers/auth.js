import { userConst } from '../constants/userConst';

export function auth(state = {}, action) {
  switch (action.type) {
    case userConst.LOGIN_REQUEST:
      return {
        loggingIn: true, 
        user: action.user
      };
    case userConst.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user 
      };
    case userConst.LOGIN_FAILURE:
      return {};
    case userConst.LOGOUT:
      return {};
    default:
      return state
  }
}