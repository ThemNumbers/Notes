import { userConst } from '../constants/userConst';

export function register(state = {}, action) {
  switch (action.type) {
    case userConst.REGISTER_REQUEST:
      return { registering: true };
    case userConst.REGISTER_SUCCESS:
      return { registeredSuccess: true };
    case userConst.REGISTER_FAILURE:
      return {};
    default:
      return state
  }
} 