export const userConst = {
        REGISTER_REQUEST: 'USERS_REGISTER_REQUEST',
        REGISTER_SUCCESS: 'USERS_REGISTER_SUCCESS',
        REGISTER_FAILURE: 'USERS_REGISTER_FAILURE',
    
        LOGIN_REQUEST: 'USERS_LOGIN_REQUEST',
        LOGIN_SUCCESS: 'USERS_LOGIN_SUCCESS',
        LOGIN_FAILURE: 'USERS_LOGIN_FAILURE',
    
        GET_REQUEST: 'GET_TASKS_REQUEST',
        GET_SUCCESS: 'GET_TASKS_SUCCESS',
        GET_FAILURE: 'GET_TASKS_FAILURE',
    
        REMOVE_REQUEST: 'REMOVE_TASK_REQUEST',
        REMOVE_SUCCESS: 'REMOVE_TASK_SUCCESS',
        REMOVE_FAILURE: 'REMOVE_TASK_FAILURE',

        ADD_REQUEST: 'ADD_TASK_REQUEST',
        ADD_SUCCESS: 'ADD_TASK_SUCCESS',
        ADD_FAILURE: 'ADD_TASK_FAILURE',

        STATE_REQUEST: 'STATE_TASK_REQUEST',
        STATE_SUCCESS: 'STATE_TASK_SUCCESS',
        STATE_FAILURE: 'STATE_TASK_FAILURE',
    
        EDIT_REQUEST: 'EDIT_TASK_REQUEST',
        EDIT_SUCCESS: 'EDIT_TASK_SUCCESS',
        EDIT_FAILURE: 'EDIT_TASK_FAILURE',

        CHANGE_TITLE: 'CHANGE_TASK_TITLE',
        CHANGE_BODY:  'CHANGE_TASK_BODY',
    
        LOGOUT: 'USERS_LOGOUT',
    };
