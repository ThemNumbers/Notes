import Axios from 'axios';
import { history } from '../helpers/history';

export const Service = {
    login,
    logout,
    register,
    addTask,
    getTasks,
    removeTask,
    editTask,
    editState
};

let defaultURL = 'http://localhost:3000/api/'

function login(email, password) {
    return Axios.post( defaultURL +'Users/login', {
        email: email,
        password: password
    })
    .then(function(response){
        localStorage.setItem('token', JSON.stringify(response.data.id));
        console.log(response)
        history.push('/main');
        return response
    })
    .catch(function (error) {
        console.log(error);
        alert('Неправильный логин или пароль!')
      });
}

function logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('tasks');
    localStorage.removeItem('edited_task')
}

function register(user) {
    return Axios.post(defaultURL + 'Users' ,  {
        username: user.username,
        email: user.email,
        password: user.password
    })
    .then(function(response){
        history.push('/auth');
        console.log(response)
    })
    .catch(function (error) {
        console.log(error);
        alert('Данный пользователь уже существует или введен неправильный email!')
      });
}

function addTask( token, title , body , done) {
    return Axios.post(defaultURL + 'tasks?access_token=' + token,  {
        title: title,
        body: body,
        done: done
    })
    .then(function(response){
        console.log(response)
        getTasks(token)
        alert('Заметка успешно добавлена!')
    })
    .catch(function (error) {
        console.log(error);
        alert('Ошибка')
      });
}

function getTasks( token ) {
    return Axios.get(defaultURL + 'tasks?access_token=' + token)
    .then(function(response){
       localStorage.setItem('tasks', JSON.stringify(response.data));
        console.log(response)
    })
    .catch(function (error) {
        console.log(error);
        alert('Ошибка')
      });
}

function removeTask(token , id) {
    return Axios.delete(defaultURL +'tasks/' + id + '?access_token=' + token)
    .then(function(response){
        localStorage.removeItem('tasks', id)
        getTasks(token)
        console.log(response)
        location.reload()
    })
    .catch(function (error) {
        console.log(error);
        alert('Ошибка')
      });
}

 function editTask(task, token) {
    return Axios.put(defaultURL + 'tasks/' + task.id + '?access_token=' + token, {
        title: task.title,
        body: task.body,
        done: task.done
    })
        .then(function (response) {
            localStorage.removeItem('tasks', task.id)
            localStorage.removeItem('edited_task')
            getTasks(token)
            location.reload()
            console.log(response)
        })
        .catch(function (error) {
            console.log(error);
            alert('Ошибка!') 
        });
}

function editState(task, token) {
    console.log(task)
    return Axios.put(defaultURL + 'tasks/' + task.id + '?access_token=' + token, {
        title: task.title,
        body: task.body,
        done: task.done
    })
        .then(function (response) {
            localStorage.removeItem('tasks', task.id)
            getTasks(token)
            console.log(response)
        })
        .catch(function (error) {
            console.log(error);
            alert('Ошибка!') 
        });
}



