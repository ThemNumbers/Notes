import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import {Actions} from '../actions/Actions'
import {bindActionCreators} from 'redux';
import Auth from './Auth';
import EditTask from './EditTask';

function Task(props) {
  return  <label	className='title'>
    {props.title}
    <label	className='body'>
      {props.body}
    </label>
  </label>         
}

export default class Notes extends Component {

  constructor() {
    super();
    let data = JSON.parse(localStorage.getItem('tasks'));
      this.state = {
      data: data,
      onEditTaskRedirect: false
    }
  }

   pushCheckbox(data){
      if (data.done == true){
        data.done = false
      } else {
        data.done = true
      }
      let token = JSON.parse(localStorage.getItem('token'));
      Actions.editState(data , token)
   }

  removeTask(id){
    let token = JSON.parse(localStorage.getItem('token'));
    Actions.removeTask(token , id)
  }

  editTask = (data) =>{
    localStorage.setItem('edited_task', JSON.stringify(data));
    this.setState({onEditTaskRedirect: true})
}

  render() {    

    let token = JSON.parse(localStorage.getItem('token'));
    if (!token){
      return <Redirect push to= '/' Component= {Auth}/>
    }
    if (token){
      Actions.getTasks(token);
    }

    if (this.state.onEditTaskRedirect){
      console.log('render')
      return <Redirect push to= '/edit_task' Component= {EditTask}/>
    }


    return <div>
      <h3>NOTES</h3>
      <Link to='/main'>Добавить заметку</Link> {
        this.state.data.map((data) => 
          <form className='notes' key={data.id}>
            <div className='tasks' key={data.id}>
              <label className='container'>
                <input type='checkbox' 
                  defaultChecked={data.done} 
                  onClick={() => { this.pushCheckbox(data)}}/> 
                <span className='checkmark'></span>
              </label>
              <Task
                key={data.id}
                title={data.title}
                body={data.body}
                id={data.id}
              />
              <button className='btnRemove' onClick={() => { this.removeTask(data.id) }}>Удалить</button>
              <button className='btnEdit' onClick={() => { this.editTask(data) }}>Изменить</button>
            </div>
          </form>     
        )}
    </div>
  }
}

function mapDispatchToProps(dispatch) {
  return {
    Actions: bindActionCreators(Actions, dispatch)
  }
}

const connectedNotes = connect(mapDispatchToProps )(Notes);
export { connectedNotes as Notes }; 

