import React, { Component } from 'react'
import { Notes } from './Notes';
import {Actions} from '../actions/Actions'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom'
import Auth from './Auth';
import UndoRedo from '../containers/UndoRedo';
import { ActionCreators as UndoActionCreators } from 'redux-undo';

export default class EditTask extends Component {

    
  constructor() {
    super();
    let data = JSON.parse(localStorage.getItem('edited_task'));
    this.state = {
        task: {
            title: data.title,
            body: data.body,
            done: data.done,
            id: data.id
          },
        titleIsEmpty: false,
        bodyIsEmpty: false,
        saveChangeRedirect: false,
        taskChanged: false,
        backToNotes: false
    }
}

    onFieldInput(fieldName, e) {
        if (e.target.value.trim().length > 0) {
          this.setState({[''+fieldName]:false})
        } else {
          this.setState({[''+fieldName]:true})
        }
      }

    onFieldChange(e) {
        const { name, value } = e.target;
        const { task } = this.state;
        if (name == 'title'){ 
            Actions.changeTaskTitle(value)
        } else {
            Actions.changeTaskBody(value)
        }
        this.setState({task: {...task, [name]: value}})
        this.setState({ taskChanged: true });
    }

    onBackBtnClick = () =>{
        if (this.state.taskChanged) {
            let result = confirm('Сохранить изменения?')
            console.log(result)
           if (result){
               this.onSaveTask()
           }else{
                this.setState({backToNotes: true})
           }
        } else{
            this.setState({backToNotes: true})
        }
    }  

    onSaveTask = () => {
        const { task } = this.state;
        let token = JSON.parse(localStorage.getItem('token'));
        console.log(task)
        Actions.editTask(task, token)
        UndoActionCreators.clearHistory()
        this.setState({ saveChangeRedirect: true })
    }; 

    render() {

        let token = JSON.parse(localStorage.getItem('token'));

        if (!token){
          return <Redirect push to= '/' Component= {Auth}/>
        } 

        if (this.state.saveChangeRedirect || this.state.backToNotes){
            return <Redirect push to= '/notes' Component= {Notes}/>
        } 

        const {task} = this.state
        let { titleIsEmpty ,  bodyIsEmpty } = this.state

        return (
            <div className='main'>
                <h3>NOTES</h3>
                <p>
                    <button
                        className='btnBack' 
                        ref='back_btn'
                        onClick= {this.onBackBtnClick}>←</button>{' '}
                </p>
                <form className='edit note'>
                    <input
                        type='text'
                        className='add_title'
                        onChange= {this.onFieldChange.bind(this)}
                        onInput={this.onFieldInput.bind(this, 'titleIsEmpty')}
                        placeholder='Название'
                        defaultValue={task.title}
                        value = {task.title}
                        name = 'title'
                    />
                    <textarea
                        className='add_body'
                        onChange= {this.onFieldChange.bind(this)}
                        onInput={this.onFieldInput.bind(this, 'bodyIsEmpty')}
                        placeholder='Что вы хотели записать?'
                        defaultValue={task.body}
                        value = {task.body}
                        name = 'body'
                    ></textarea>
                     <button
                        type='button'
                        className='btnSaveTask' 
                        onClick={this.onSaveTask}
                        disabled={ titleIsEmpty || bodyIsEmpty}
                        >Сохранить
                    </button>{' '}
                </form>
                <UndoRedo/>
         </div>
            );
        }
} 
 
function mapStateToProps(state) {
    const { title, body } = state.tasks.present;
    return {
        title,
        body
    };
}

function mapDispatchToProps(dispatch) {
    return {
      Actions: bindActionCreators(Actions, dispatch)
    }
}
 
const connectedNotes = connect(mapStateToProps, mapDispatchToProps )(EditTask);
export { connectedNotes as EditTask }; 
   