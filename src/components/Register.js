import React, { Component } from 'react'
import { Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Auth from './Auth';
import {Actions} from '../actions/Actions'

export default class Register extends Component {

  state = {
    user: {
      email: '',
      password: ''
    },
    emailIsEmpty: true, 
    passwordIsEmpty: true,
    backToAuth: false
  }
  
  onFieldChange(e) {
    const { name, value } = e.target;
    const { user } = this.state;
    this.setState({user: {...user, [name]: value}})
  }

  onFieldInput(fieldName, e) {
    if (e.target.value.trim().length > 0) {
      this.setState({[''+fieldName]:false})
    } else {                                  
      this.setState({[''+fieldName]:true})
    }
  }

  onBackBtnClick = () =>{
    this.setState({backToAuth: true})
  }

  onRegisterBtnClick(e) {
    e.preventDefault();
    const { user } = this.state;
    if (user.email && user.password) {
       Actions.register(user);   
    } else
      alert('Ошибка')
  }

  render() {

  if (this.state.backToAuth){
    return <Redirect push to= '/' Component= {Auth}/>
  }

  const {user} = this.state
  let { emailIsEmpty , passwordIsEmpty}= this.state

  return <div className='register'>
    <h3>NOTES</h3>
    <input
      type='text'
      className='email'
      onChange={this.onFieldChange.bind(this)}
      onInput= {this.onFieldInput.bind(this, 'emailIsEmpty')}
      placeholder='Email'
      value = {user.email}
      name = 'email'
      ref='email'
    />
    <input
      type='password'
      className='password'
      onChange={this.onFieldChange.bind(this)}
      onInput= {this.onFieldInput.bind(this, 'passwordIsEmpty')}
      placeholder='Пароль'
      value = {user.password}
      name = 'password'
      ref='password'
    />
    <p>
      <button
        className='btnRegister' 
        onClick = {this.onRegisterBtnClick.bind(this)}
        ref='register_btn'
        disabled={emailIsEmpty || passwordIsEmpty}>Зарегестрироваться</button>{' '}
    </p>
    <p>
      <button
        className='btnBack' 
        ref='back_btn'
        onClick= {this.onBackBtnClick}>←</button>{' '}
    </p>
  </div>
  }
}

function mapDispatchToProps(dispatch) {
  return {
    Actions: bindActionCreators(Actions, dispatch)
  }
}

const connectedRegisterPage = connect( mapDispatchToProps)(Register);
export { connectedRegisterPage as Register };
