import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import Register from './Register'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Actions } from '../actions/Actions'
import { Service } from '../services/Service';

export default class Auth extends Component {

  constructor(props) {
    super(props);
      Service.logout();
  }

  state = {
    email: '',
    password: '',
    emailIsEmpty: true,
    passwordIsEmpty: true,
    registerRedirect: false,
  }

  onFieldChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
    }

    
  onFieldInput(fieldName, e) {
    if (e.target.value.trim().length > 0) {
      this.setState({[''+fieldName]:false})
    } else {                                  
      this.setState({[''+fieldName]:true})
    }
  } 

  onAuthBtnClick(e) {
    e.preventDefault();
    const { email, password } = this.state;
    if (email && password) {
      Actions.login(email, password);
      console.log(email, password); 
    } else
      alert('Ошибка')
  }

  onRegisterBtnClick = () =>{
    this.setState({registerRedirect: true})
  }

  render() {

    if (this.state.registerRedirect){
      return <Redirect push to= '/register' Component= {Register}/>
    }

    let { passwordIsEmpty , emailIsEmpty} = this.state
           
    return <div className='auth'>
      <h3>NOTES</h3>
        <input
          className='email'
          type='text'
          onChange={this.onFieldChange.bind(this)}
          onInput= {this.onFieldInput.bind(this, 'emailIsEmpty')}
          placeholder='Email'
          value = {this.state.email}
           name = 'email'
          ref='email'
        />
        <input
          className='password'
          type='password'
          onChange={this.onFieldChange.bind(this)}
          onInput= {this.onFieldInput.bind(this, 'passwordIsEmpty')}
          placeholder='Пароль'
          value = {this.state.password}
          name = 'password'
          ref='password'
        /> 
        <p>
          <button
            className='btnAuth' 
            onClick={this.onAuthBtnClick.bind(this)}
            ref='auth_btn'
            disabled={ emailIsEmpty || passwordIsEmpty}>Войти</button>{' '}
        </p>
        <p>
          <button
            className='btnRegister'
            onClick= {this.onRegisterBtnClick}
            ref='auth_btn'>Зарегистрироваться</button>{' '}
        </p>
             
      </div>
    }
}

function mapDispatchToProps(dispatch) {
  return {
    Actions: bindActionCreators(Actions , dispatch)
  }
}
        
const connectedAuth = connect( mapDispatchToProps )(Auth);
export { connectedAuth as Auth };         
        
        

  