import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { Notes } from './Notes';
import {Actions} from '../actions/Actions'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom'
import Auth from './Auth';

export default class Main extends Component {

    state = {
        title: '',
        body: '',
        done: false,
        titleIsEmpty: true,
        bodyIsEmpty: true,
        myNotesRedirect: false
    }

    onFieldInput(fieldName, e) {
        if (e.target.value.trim().length > 0) {
          this.setState({[''+fieldName]:false})
        } else {
          this.setState({[''+fieldName]:true})
        }
      }

    onFieldChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    } 

    onMyNotesBtnClick = () =>{
        console.log(localStorage.getItem('tasks'))
        if (localStorage.getItem('tasks') == null){
            alert('Заметок нет, добавте хотябы одну заметку!')
        } else
        this.setState({myNotesRedirect: true})
    }

    onAddNoteBtnClick(e) {
        e.preventDefault();
        const { title , body , done } = this.state;
        let token = JSON.parse(localStorage.getItem('token'));
        if (title && body && token) {
           Actions.addTask(token, title, body , done);  
        } else 
        alert('Ошибка')
    }

    render() {

        let token = JSON.parse(localStorage.getItem('token'));

        if (!token){
          return <Redirect push to= '/' Component= {Auth}/>
        } 

        if (this.state.myNotesRedirect){
            return <Redirect push to= '/notes' Component= {Notes}/>
          }  

        const {title , body} = this.state
        let { titleIsEmpty ,  bodyIsEmpty } = this.state

        return (
            <div className='main'>
                <h3>NOTES</h3>
                <p>
                    <Link to='/auth'>Выйти</Link>
                </p>
                <form className='add note'>
                    <input
                        type='text'
                        className='add_title'
                        onChange= {this.onFieldChange.bind(this)}
                        onInput={this.onFieldInput.bind(this, 'titleIsEmpty')}
                        placeholder='Название'
                        value = {title}
                        name = 'title'
                        ref='add_title'
                    />
                    <textarea
                        className='add_body'
                        onChange= {this.onFieldChange.bind(this)}
                        onInput={this.onFieldInput.bind(this, 'bodyIsEmpty')}
                        placeholder='Что вы хотели записать?'
                        value = {body}
                        name = 'body'
                        ref='add_body'
                    ></textarea>
                     <button
                        className='btnAddNote' 
                        onClick={this.onAddNoteBtnClick.bind(this)}
                        ref='add_note_btn'
                        disabled={ titleIsEmpty || bodyIsEmpty}
                        >Добавить заметку
                    </button>{' '}
                </form>
                <button
                    className='btnMyNotes' 
                    onClick= {this.onMyNotesBtnClick}
                    ref='my_notes_btn'
                    >Мои заметки
                </button>{' '}
         </div>
            );
        }
} 

function mapStateToProps(addSuccess) {
    return {
        addSuccess
    }
}

function mapDispatchToProps(dispatch) {
    return {
      Actions: bindActionCreators(Actions, dispatch)
    }
}
 
const connectedNotes = connect(mapStateToProps, mapDispatchToProps )(Main);
export { connectedNotes as Main }; 