import React, { Component } from 'react'
import Main from '../components/Main';
import { Route , HashRouter , Switch} from 'react-router-dom';
import { PrivateRoute } from '../components/PrivateRoute';
import Auth from '../components/Auth';
import Register from '../components/Register';
import { Notes } from '../components/Notes';
import EditTask from '../components/EditTask';

export default class App extends Component {

  render() {
    return (
        <div className='row'> 
        <HashRouter>
            <div>
            <Switch>
              <PrivateRoute exact path='/' component={Main} /> 
              <Route path= '/auth' component= {Auth}></Route>
              <Route path = '/register' component= {Register}></Route>
              <Route path= '/main' component= {Main}></Route>
              <Route path= '/notes' component= {Notes}></Route>
              <Route path= '/edit_task' component= {EditTask}></Route>
            </Switch>
            </div>
        </HashRouter>
        </div>
    );
}
}