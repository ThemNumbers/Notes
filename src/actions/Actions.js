import {Service} from '../services/Service'
import { userConst } from '../constants/userConst'

export const Actions = {
    login,
    register,
    getTasks,
    removeTask,
    editTask,
    editState,
    addTask,
    changeTaskBody,
    changeTaskTitle
};

function register(user) {{
        request(user);

        Service.register(user)
            .then(
                user => {
                    console.log(user)
                    if (typeof (user) !== 'undefined') {
                        success();
                    }
                },
                error => {
                    failure(error);
                    console.log(error)
                }
            );
}

    function request(user) { return { type: userConst.REGISTER_REQUEST, user } }
    function success() { return { type: userConst.REGISTER_SUCCESS } }
    function failure(error) { return { type: userConst.REGISTER_FAILURE, error } }
}


function login(email, password) {{
        request();

        Service.login(email, password)
            .then(
                user => {
                    if (typeof (user) !== 'undefined') {
                        success(user);
                    }
                },
                error => {
                    failure(error);
                    console.log(error)

                }
            );
    }

    function request() { return { type: userConst.LOGIN_REQUEST } }
    function success(user) { return { type: userConst.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConst.LOGIN_FAILURE, error } }
}


function getTasks(token) {{
        request();

        Service.getTasks(token)
            .then(
                tasks => {
                    if (typeof (tasks) !== 'undefined') {
                        success(tasks);
                    }
                },
                error => {
                    failure(error);
                    console.log(error)
                }
            );
}

    function request() { return { type: userConst.GET_REQUEST } }
    function success(tasks) { return { type: userConst.GET_SUCCESS, tasks } }
    function failure(error) { return { type: userConst.GET_FAILURE, error } }
}


function removeTask(token, id) {{
        request();

        Service.removeTask(token, id)
            .then(
                response => {
                    console.log(response)
                    if (typeof (response) !== 'undefined') {
                        success(id);
                    }
                },
                error => {
                    failure(error);
                    console.log(error)

                }
            );
}

    function request() { return { type: userConst.REMOVE_REQUEST } }
    function success(id) { return { type: userConst.REMOVE_SUCCESS, id } }
    function failure(error) { return { type: userConst.REMOVE_FAILURE, error } }
}


function editTask(task , token) {{
        request();

        Service.editTask(task, token)
            .then(
                response => {
                    console.log(response)
                    if (typeof (response) !== 'undefined') {
                        success(response);
                    }
                },
                error => {
                    failure(error);
                    console.log(error)
                }
            );
    }

    function request() { return { type: userConst.EDIT_REQUEST } }
    function success(response) { return { type: userConst.EDIT_SUCCESS, response } }
    function failure(error) { return { type: userConst.EDIT_FAILURE, error } }
}


function editState(task , token) {{
        request();

        Service.editState(task, token)
            .then(
                response => {
                    console.log(response)
                    if (typeof (response) !== 'undefined') {
                        success(response);
                    }
                },
                error => {
                    failure(error);
                    console.log(error)
                }
            );
    }

    function request() { return { type: userConst.STATE_REQUEST } }
    function success(response) { return { type: userConst.STATE_SUCCESS, response } }
    function failure(error) { return { type: userConst.STATE_FAILURE, error } }
}

function addTask(token, title, body, done ) {{
        request();

        Service.addTask(token, title, body, done)
            .then(
                response => {
                    console.log(response)
                    if (typeof (response) !== 'undefined') {
                        success(response);
                    }
                },
                error => {
                    failure(error);
                    console.log(error)
                }
            );
}
    function request() { return { type: userConst.ADD_REQUEST } }
    function success(response) { return { type: userConst.ADD_SUCCESS, response } }
    function failure(error) { return { type: userConst.ADD_FAILURE, error } }
}

function changeTaskTitle(title) {{
        showTaskAction(title)
    }
    function showTaskAction(title) { return { type: userConst.CHANGE_TITLE, title } }
}

function changeTaskBody(body) {{
        showTaskAction(body)
    }
    function showTaskAction(body) { return { type: userConst.CHANGE_BODY, body } }
}